package dns;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import org.apache.log4j.Logger;

public class NetUtils {
	static Logger logger = Logger.getLogger(NetUtils.class);
	//获取当前最新IP
	public static String getNewIP(String uri) {
		URL url = null;
		BufferedReader in = null;
		StringBuffer sb = new StringBuffer();
		try {
			url = new URL(uri);
			in = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
			String str = null;
			while ((str = in.readLine()) != null) {
				sb.append(str);
			}
		} catch (Exception e) {
			logger.error("--- 访问 "+uri+" 出错,无法获取新IP ---");
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				logger.error("--- 访问 "+uri+" 出错,无法关闭数据流 ---");
			}
		}
		String result = sb.toString();
		return result;
	}
	
	//执行 curl 命令
	public static String execCurl(String[] cmds) {
        ProcessBuilder process = new ProcessBuilder(cmds);
        Process p;
        try {
            p = process.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append(System.getProperty("line.separator"));
            }
            return builder.toString();
 
        } catch (IOException e) {
        	logger.error("--- curl 请求数据错误 ---");
        }
        return null;
    }

}

package dns;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import org.apache.log4j.Logger;

public class ReadWriteFile {
	static Logger logger = Logger.getLogger(ReadWriteFile.class);
	//逐行读取IP文件
	public static String readTxt(String path) {
		File file = new File(path);
		String result = "";
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String s = null;
			while((s = br.readLine())!=null){
				result = result + s;
			}
			br.close();
		}catch(Exception e) {
			logger.error("--- readTxt() 无法读取IP文件 ---");
		}
		return result;
	}
	//覆盖写入IP文件
	public static void writeTxt(String ip,String path) {
		File file = new File(path);
		try {
			Writer bw=new FileWriter(file,false);
			if(file.exists()) {
				//Runtime.getRuntime().exec("sudo chmod 766 -R " + path);
			}else {
				file.createNewFile();
				//Runtime.getRuntime().exec("sudo chmod 766 -R " + path);
			}
			bw.write(ip);	
			bw.close();
		} catch (Exception e) {
			logger.error("--- writeTxt() 无法写入IP文件 ---");
		}
	}
}
